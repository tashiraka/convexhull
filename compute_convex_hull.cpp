#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/point_generators_3.h>
#include <CGAL/algorithm.h>
#include <CGAL/Polyhedron_3.h>
#include <CGAL/convex_hull_3.h>
#include <vector>
#include <fstream>

typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
typedef CGAL::Polyhedron_3<K> Polyhedron_3;
typedef K::Segment_3 Segment_3;
typedef K::Point_3 Point_3;


//a functor computing the plane containing a triangular facet
/*
struct Plane_from_facet {
  Polyhedron_3::Plane_3 operator()(Polyhedron_3::Facet& f) {
    Polyhedron_3::Halfedge_handle h = f.halfedge();
    return Polyhedron_3::Plane_3( h->vertex()->point(),
                                  h->next()->vertex()->point(),
                                  h->opposite()->vertex()->point());
  }
};
*/


int main(int argc, char** argv)
{
  if(argc != 3) {
    std::cout << "Usage: " << argv[0]
              << " <pints file> <out file>" << std::endl;
    return 1;
  }

  char* pointsFile = argv[1];
  char* outFile = argv[2];  

  // input points
  std::ifstream fin(pointsFile);
  std::list<Point_3> points;
  Point_3 p;
  while(fin >> p)
    points.push_back(p);

  Polyhedron_3 poly;
  
  // compute convex hull of non-collinear points
  CGAL::convex_hull_3(points.begin(), points.end(), poly);

  std::ofstream fout(outFile);

  for(Polyhedron_3::Facet_iterator it = poly.facets_begin();
      it != poly.facets_end(); ++it)
    {
      Polyhedron_3::Halfedge_handle h = it->halfedge();
      Point_3 p1 = h->vertex()->point();
      Point_3 p2 = h->next()->vertex()->point();
      Point_3 p3 = h->opposite()->vertex()->point();
      fout << p1[0] << "," << p1[1] << "," << p1[2] << "\t"
           << p2[0] << "," << p2[1] << "," << p2[2] << "\t"
           << p3[0] << "," << p3[1] << "," << p3[2] << "\n";
    }
  

  // assign a plane equation to each polyhedron facet
  // using functor Plane_from_facet
  /*
  std::transform( poly.facets_begin(), poly.facets_end(),
                  poly.planes_begin(), Plane_from_facet());
  */
  
  return 0;
}
