import std.stdio, std.conv, std.string, std.algorithm, std.array, std.math;


version(unittest){}
 else{
   void main(string[] args)
   {
     auto inFile = args[1];

     double[][] points;
     foreach(line; File(inFile, "r").byLine) {
       points ~= line.to!string.strip.split("\t").map!(x => x.to!double).array;
     }

     double diameter = - 1.0;
     foreach(p1; points) {
       foreach(p2; points) {
         diameter = max(diameter, dist(p1, p2));
       }
     }

     writeln(diameter);
   }
 }


double dist(double[] p1, double[] p2)
{
  double dist = 0;
  foreach(i; 0..p1.length) {
    dist += (p1[i] - p2[i])^^2;
  }
  return dist.sqrt;
}
