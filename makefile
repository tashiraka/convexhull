GCC = g++
CPPFLAGS = -std=c++11 -O3
CPPSRC = $(wildcard *.cpp)
CPPOBJ = $(CPPSRC:%.cpp=%.o)
CPPEXE = $(CPPSRC:%.cpp=%)

DMD = dmd
DFLAGS = -O -release -inline -noboundscheck
TESTDFLAGS = -unittest -main
DSRC = $(wildcard *.d)
DOBJ = $(DSRC:%.d=%.o)
DEXE = $(DSRC:%.d=%)


.PHONY: all clean

all: $(CPPEXE) $(DEXE)

compute_convex_hull: compute_convex_hull.cpp
	$(GCC) $(CPPFLAGS) $^ -lCGAL -lgmp -lmpfr -lm -o $@

dot_on_convex_hull: dot_on_convex_hull.d
	$(DMD) $(DFLAGS) $^

test:
	$(RDMD) $(TESTDFLAGS) $(DSRC)

clean:
	rm *~ $(CPPOBJ) $(CPPEXE) $(DOBJ) $(DEXE)

