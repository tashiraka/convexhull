compute_convex_hull.cpp
3 次元上の点集合の convex hull を作成する。 Convex hull は三角形の集合で表される。

dot_on_convex_hull.d
作成した convex hull 上に点を格子状に敷き詰める。この格子点に convex hull の頂点は
含まないものとする。計算精度の問題で格子点が coonvex hull の内側に点が作られる場合がある。
格子点が内側に作られてしまった点は、convex hull の外側に移動させる。

compute_diameter.d
点集合の直径を計算する。
