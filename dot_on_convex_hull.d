import std.stdio, std.conv, std.string, std.math, std.algorithm, std.array;

version(unittest){}
 else{
   void main(string[] args)
   {
     auto convexHullFile = args[1];
     auto densityFactor = args[2].to!double;
     auto outFile = args[3];

     auto facets = readConvexHull(convexHullFile);
     auto pointOnConvexHull = dotOnFacets(facets, densityFactor);

     auto fout = File(outFile, "w");
     foreach(point; pointOnConvexHull) {
       fout.writeln(point.x, "\t", point.y, "\t", point.z);
     }
   }
 }


Point[] dotOnFacets(Point[3][] facets, double densityFactor) {
  /* 
   * create the center point of triangles recursively.
   * create the center point of edges recursively. 
   */
  Point[] points;

  foreach(facet; facets) {
    points ~= centerManyTimes(facet[0], facet[1], facet[2], densityFactor);
    points ~= centerManyTimes(facet[0], facet[1], densityFactor * 30);
    points ~= centerManyTimes(facet[1], facet[2], densityFactor * 30);
    points ~= centerManyTimes(facet[2], facet[0], densityFactor * 30);
  }
  return points;
}


double euclideanNorm(Point p)
{
  return sqrt(p.x^^2 + p.y^^2 + p.z^^2);
}


Point outerProduct(Point p1, Point p2)
{
  return new Point(p1.y * p2.z - p1.z * p2.y,
                   p1.z * p2.x - p1.x * p2.z,
                   p1.x * p2.y - p1.y * p2.x);
}


double area(Point p1, Point p2, Point p3)
{
  return outerProduct(p2 - p1, p3 - p1).euclideanNorm / 2;
}


Point center(Point[] points)
{
  return reduce!((a, b) => a + b)(new Point(0, 0, 0), points)
    / points.length.to!double;
}


double euclideanDist(Point p1, Point p2)
{
  return euclideanNorm(p2 - p1);
}


Point[] centerManyTimes(Point p1, Point p2, Point p3, double densityFactor)
{
  Point[] points;
  Point[3][] facets = [[p1, p2, p3]];
  auto targetPointNum = area(p1, p2, p3) * densityFactor - 1;

  while(points.length < targetPointNum) {
    Point[3][] newFacets;

    foreach(facet; facets) {
      auto centerPoint = facet.center;
      newFacets ~= [[centerPoint, facet[0], facet[1]],
                    [centerPoint, facet[1], facet[2]],
                    [centerPoint, facet[2], facet[0]]];
      points ~= centerPoint;
    }

    facets = newFacets;
  }

  return points;
}


Point[] centerManyTimes(Point p1, Point p2, double densityFactor)
{
  Point[] points;
  Point[2][] edges = [[p1, p2]];
  auto targetPointNum = euclideanDist(p1, p2) * densityFactor - 1;

  while(points.length < targetPointNum) {
    Point[2][] newEdges;

    foreach(edge; edges) {
      auto centerPoint = edge.center;
      newEdges ~= [[centerPoint, edge[0]],
                    [centerPoint, edge[1]]];
      points ~= centerPoint;
    }

    edges = newEdges;
  }

  return points;
}


unittest {
  auto p1 = new Point(0, 0, 0);
  auto p2 = new Point(1, 1, 1);
  auto p = centerManyTimes(p1, p2, 1);
  assert(p.length == 1);
  assert(p[0].x == 0.5);
  assert(p[0].y == 0.5);
  assert(p[0].z == 0.5);
}


Point[3][] readConvexHull(string filename)
{
  Point[3][] facets;
  foreach(line; File(filename, "r").byLine) {
    auto fields = line.to!string.strip.split("\t");
    auto p1 = fields[0].split(",").map!(x => x.to!double).array;
    auto p2 = fields[1].split(",").map!(x => x.to!double).array;
    auto p3 = fields[2].split(",").map!(x => x.to!double).array;
    facets ~= [new Point(p1[0], p1[1], p1[2]),
               new Point(p2[0], p2[1], p2[2]),
               new Point(p3[0], p3[1], p3[2])];
  }
  return facets;
}


class Point
{
  double x, y, z;

  this(double a, double b, double c) {
    x = a; y = b, z = c;
  }
  
  Point opBinary(string op)(Point p) {
    static if(op == "+") return new Point(x + p.x, y + p.y, z + p.z);
    else static if(op == "-") return new Point(x - p.x, y - p.y, z - p.z);
    else static assert(0, "Operator "~op~" not implemented");
  }
  Point opBinary(string op)(double a) {
    static if(op == "/") return new Point(x / a, y / a, z / a);
    else static if(op == "*") return new Point(x * a, y * a, z * a);
    else static assert(0, "Operator "~op~" not implemented");
  }

  Point opBinaryRight(string op)(double a) {
    static if(op == "/") return new Point(x / a, y / a, z / a);
    else static if(op == "*") return new Point(x * a, y * a, z * a);
    else static assert(0, "Operator "~op~" not implemented");
  }
}

